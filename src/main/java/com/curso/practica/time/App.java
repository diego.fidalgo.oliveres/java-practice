package com.curso.practica.time;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class App {

	public static void main(String[] args) {
		App.localDate();
		System.out.println("---------------------");
		App.localTime();
		System.out.println("---------------------");
		App.formatTime();
		System.out.println("---------------------");		
		App.localDateTime();
	}
	
	public static void localDate() {
		LocalDate fechaHoy = LocalDate.now();
		System.out.println("Día del año: "+fechaHoy.getDayOfYear());
		System.out.println("Día de la semana: "+fechaHoy.getDayOfWeek());
		System.out.println("Nombre del mes: "+fechaHoy.getMonth());
	}

	public static void localTime() {
		LocalTime tiempoAhora = LocalTime.now();
		System.out.println("Hora: "+tiempoAhora.getHour());
		System.out.println("Segundo del día: "+tiempoAhora.toSecondOfDay());
		System.out.println("Dentro de 5h serán las: "+tiempoAhora.plusHours(5).toString());
	}
	
	public static void formatTime() {
		String dateStringFormat = "2021/11/17";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		try {
		Date parsedate = sdf.parse(dateStringFormat);
		System.out.println("Estamos a: " + parsedate);
		}
		catch (ParseException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void localDateTime() {
		LocalDateTime finDeAño = LocalDateTime.of(2021, 12, 31, 23, 59);
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		System.out.println("Fecha: "+ finDeAño.format(dateFormat));
		DateTimeFormatter timeFormat = DateTimeFormatter.ofPattern("hh:mm:ss");
		System.out.println("Hora: "+ finDeAño.format(timeFormat));
	}
}
