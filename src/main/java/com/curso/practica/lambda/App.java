package com.curso.practica.lambda;

import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class App {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		App.ejercicioUno();
		App.ejercicioDos();
		App.consumer();
		App.supplier();
		App.function();
		App.predicate();
	}
	
	public static void ejercicioUno() {
		Integer x = 5;
		ej1 ej = (y) -> y+5;
		System.out.println(ej.addFive(x));
	}

	public static void ejercicioDos() {
		ej2 ej = () -> new Random().nextInt(100);
		System.out.println(ej.returnNumber());
	}
	
	public static void consumer() {
		String texto = "Hola mundo!";
		Consumer <String>c = (String x) -> System.out.println(x);
		c.accept(texto);
	}
	
	public static void supplier() {
		Supplier<Integer> getTen = () -> 10;
		System.out.println(getTen.get());
	}
	
	public static void function() {
		String textStraight = "Invierte esto.";
		Function<String, String> reverseString = text -> {
			String reversedText = "";
			for(int i = text.length()-1; i >= 0 ;--i ) {
				reversedText = reversedText + text.charAt(i);
			}
			return reversedText;
		};
		String result = reverseString.apply(textStraight);
		System.out.println(result);
	}
	
	public static void predicate() {
		Predicate<Integer> divisibleByFour = num -> num%4 == 0;
		System.out.println(divisibleByFour.test(35));
		System.out.println(divisibleByFour.test(36));
	}
}
