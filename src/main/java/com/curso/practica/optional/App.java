package com.curso.practica.optional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


public class App {

	public static void main(String[] args) {
		Conductor c1 = new Conductor("Nadia", 27);
		Conductor c2 = new Conductor("Diego", 26);
		Conductor c3 = new Conductor("Ismael", 24);
		
		Coche coche1 = new Coche("1234abc");
		Coche coche2 = new Coche("4321cba");
		Coche coche3 = new Coche("2321fba");
		Coche coche4 = new Coche("1221dba");
		
		coche1.setConductor(Optional.of(c1));
		coche2.setConductor(Optional.of(c1));
		coche3.setConductor(Optional.of(c2));
		coche4.setConductor(Optional.empty());
		
		c1.setCoche(Optional.of(coche1));
		c2.setCoche(Optional.of(coche3));
		
		List <Coche> coches = new ArrayList<Coche>();
		coches.add(coche1);
		coches.add(coche2);
		coches.add(coche3);
		coches.add(coche4);
		
		List <Conductor> conductores = new ArrayList<Conductor>();
		conductores.add(c1);
		conductores.add(c2);
		conductores.add(c3);
		
		App.busqueda(coches);
		System.out.println("----------------------");
		App.bucleForEach(coches);
		System.out.println("----------------------");
		App.busquedaOptionals(coches);
	}
	
	public static void busqueda(List<Coche> coche) {
		Optional <Conductor> c1 = coche.get(3).getConductor();
		if(c1.isPresent()) {
			System.out.println(c1.get().getNombre());
		}else {
			System.out.println("No existe ningun conductor en este coche.");
		}
		
		Optional <Conductor> c2 = coche.get(0).getConductor();
		if(c2.isPresent()) {
			System.out.println(c2.get().getNombre());
		}else {
			System.out.println("No existe ningun conductor en este coche.");
		}
	}
	
	public static void bucleForEach(List<Coche> coche) {
		Stream <Coche> coches = coche.stream();
		coches.forEach(c -> {if(c.getConductor().isPresent()) 
			System.out.println(c.getConductor().get().getNombre());
		else
			System.out.println("No existe ningun conductor en este coche.");});
	}
	
	public static void busquedaOptionals(List<Coche> coche) {
		String nombre = "Diego";
		Stream <Coche> coches = coche.stream();
		coches.filter((c) -> c.getConductor().isPresent())
		.filter((c) -> c.getConductor().get().getNombre() == nombre)
		.forEach(c -> System.out.println(c.getNombre()));
		
	}
}
