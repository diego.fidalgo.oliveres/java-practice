package com.curso.practica.optional;

import java.util.Comparator;
import java.util.Optional;

public class Conductor implements Comparable<Conductor> {

	public String nombre;
	public Integer edad;
	public Optional<Coche> coche;
	
	public Conductor(String nombre, Integer edad, Optional<Coche> coche) {
		this.nombre = nombre;
		this.edad = edad;
		this.coche = coche;
	}
	
	public Conductor(String nombre, Integer edad) {
		this.nombre = nombre;
		this.edad = edad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdad() {
		return edad;
	}

	public void setEdad(Integer edad) {
		this.edad = edad;
	}

	public Optional<Coche> getCoche() {
		return coche;
	}

	public void setCoche(Optional<Coche> coche) {
		this.coche = coche;
	}

	@Override
	public String toString() {
		return "Conductor [nombre=" + nombre + ", edad=" + edad +"]";
	}
	
	public int compareTo(Conductor c) {
		return edad - c.getEdad();
	}
	
}
