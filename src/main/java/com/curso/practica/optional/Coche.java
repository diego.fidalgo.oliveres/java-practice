package com.curso.practica.optional;

import java.util.Objects;
import java.util.Optional;

public class Coche {

	private String matricula;
	private Optional<Conductor> conductor;
	
	public Coche(String matricula, Optional<Conductor> conductor) {
		this.matricula = matricula;
		this.conductor = conductor;
	}
	
	public Coche(String matricula) {
		this.matricula = matricula;
		this.conductor = null;
		
		
	}

	public String getNombre() {
		return matricula;
	}

	public void setNombre(String nombre) {
		this.matricula = nombre;
	}

	public Optional<Conductor> getConductor() {
		return conductor;
	}

	public void setConductor(Optional<Conductor> conductor) {
		this.conductor = conductor;
	}

	@Override
	public String toString() {
		return "Coche [matricula=" + matricula + "]";
	}
}
