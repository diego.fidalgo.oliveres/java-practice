package com.curso.practica.stream;

import java.util.Objects;

public class Coche {

	private String matricula;
	private Conductor conductor;
	
	public Coche(String matricula, Conductor conductor) {
		this.matricula = matricula;
		this.conductor = conductor;
	}
	
	public Coche(String matricula) {
		this.matricula = matricula;
		this.conductor = null;
		
		
	}

	public String getNombre() {
		return matricula;
	}

	public void setNombre(String nombre) {
		this.matricula = nombre;
	}

	public Conductor getConductor() {
		return conductor;
	}

	public void setConductor(Conductor conductor) {
		this.conductor = conductor;
	}

	@Override
	public String toString() {
		return "Coche [matricula=" + matricula + "]";
	}
}
