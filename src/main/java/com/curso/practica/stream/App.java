package com.curso.practica.stream;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


public class App {

	public static void main(String[] args) {
		Conductor c1 = new Conductor("Nadia", 27);
		Conductor c2 = new Conductor("Diego", 26);
		Conductor c3 = new Conductor("Ismael", 24);
		
		Coche coche1 = new Coche("1234abc");
		Coche coche2 = new Coche("4321cba");
		Coche coche3 = new Coche("2321fba");
		Coche coche4 = new Coche("1221dba");
		
		coche1.setConductor(c1);
		coche2.setConductor(c1);
		coche3.setConductor(c2);
		coche4.setConductor(c2);
		
		c1.setCoche(coche1);
		c2.setCoche(coche2);
		
		List <Coche> coches = new ArrayList<Coche>();
		coches.add(coche1);
		coches.add(coche2);
		coches.add(coche3);
		coches.add(coche4);
		
		List <Conductor> conductores = new ArrayList<Conductor>();
		conductores.add(c1);
		conductores.add(c2);
		conductores.add(c3);
		
		App.bucleForEach(conductores);
		System.out.println("-----------------");
		App.bucleSTream(conductores);
		System.out.println("-----------------");
		App.filtradoConcatenado(conductores);
		System.out.println("-----------------");
		App.forIterator(conductores);
		System.out.println("-----------------");		
		App.comparatorMax(conductores);
		App.comparatorMin(conductores);
		System.out.println("-----------------");		
		App.distinct(coches);
	}
	
	public static void bucleForEach(List<Conductor> conductores) {
		conductores.forEach((Conductor c) -> System.out.println(c.getNombre()));
	}

	public static void bucleSTream(List<Conductor> conductores) {
		Stream<Conductor> streamC = conductores.stream();
		streamC.forEach((Conductor c) -> System.out.println(c.getNombre()));
	}
	
	public static void filtradoConcatenado(List<Conductor> conductores) {
		Stream<Conductor> streamC = conductores.stream();
		streamC.filter((Conductor c) -> c.getCoche() != null).filter((Conductor c) -> c.getEdad() > 26)
		.forEach((Conductor c) -> System.out.println(c.getNombre()));;
	}
	
	public static void forIterator(List<Conductor> conductores) {
		Iterator<Conductor> iterator = conductores.stream().iterator();
		while(iterator.hasNext()) {
			System.out.println(iterator.next().getNombre());
		}
	}
	
	public static void comparatorMax(List<Conductor> conductores) {
		Stream<Conductor> streamC = conductores.stream();
		Comparator<Conductor> comparator = new Comparator<Conductor>() {
			public int compare(Conductor c1, Conductor c2) {
				return c1.getEdad() - c2.getEdad();
			}
		};
		
		Optional<Conductor> cMax = streamC.max(comparator);
		if(cMax.isPresent()) {
			System.out.println("Conductor con mayor edad: "+ cMax.get().getNombre());
		}
	}
	
	public static void comparatorMin(List<Conductor> conductores) {
		Stream<Conductor> streamC = conductores.stream();
		Comparator<Conductor> comparator = new Comparator<Conductor>() {
			public int compare(Conductor c1, Conductor c2) {
				return c1.getEdad() - c2.getEdad();
			}
		};
		
		Optional<Conductor> cMin = streamC.min(comparator);
		if(cMin.isPresent()) {
			System.out.println("Conductor con menor edad: "+ cMin.get().getNombre());
		}
	}
	
	public static void distinct(List<Coche> coches) {
		Stream<Coche> streamCoche = coches.stream();
		streamCoche.map(coche -> coche.getConductor()).forEach(conductor -> System.out.println(conductor.getNombre()));
		Stream<Coche> streamCocheDistinct = coches.stream();
		System.out.println("----Distinct----");
		streamCocheDistinct.map(coche -> coche.getConductor()).distinct().forEach(conductor -> System.out.println(conductor.getNombre()));
	}
}
